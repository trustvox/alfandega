package alfandega

func min(x, y uint) uint {
	if x < y {
		return x
	}

	return y
}

func max(x, y uint) uint {
	if x > y {
		return x
	}

	return y
}
