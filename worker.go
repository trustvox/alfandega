package alfandega

import (
	"strings"
	"sync"

	"github.com/tealeg/xlsx"
)

// Payload describes the return type of the parsing channel
type Payload []string

// Worker represent a parsing agent
type Worker struct {
	id      uint
	from    uint
	to      uint
	channel chan Payload
}

// CreateWorker creates a worker definition based on the current worker id.
// It calculates the initial and final range for a given chunk value.
// Example:
// For chuckEvery equals to ten, it will output as follows:
// id zero						 		 id one										id two
// from := 0 <- (0 * 10)	 from := 10 <- (1 * 10)		from := 20 <- (2 * 10)
// to	:= 10 <- (0 + 10)		 to := 20 <- (10 + 10)		to := 30 <- (20 + 10)
func CreateWorker(id, chunkEvery uint) Worker {
	from := id * chunkEvery
	to := from + chunkEvery

	return Worker{id: id, from: from, to: to, channel: make(chan Payload)}
}

// Parse extracts the content from a spreadsheet range
func (w Worker) Parse(sheet *xlsx.Sheet, handler *sync.WaitGroup) {
	defer handler.Done()

	for i := w.from; i < w.to; i++ {
		row := sheet.Rows[i]
		content := make([]string, len(row.Cells))

		for index, cell := range row.Cells {
			value, error := cell.FormattedValue()

			if error == nil {
				content[index] = strings.Trim(value, " ")
			} else {
				content[index] = error.Error()
			}
		}

		w.channel <- Payload(content)
	}
}
