package alfandega

import (
	"io"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"
)

var testfile = Spreadsheet("testdata/testfile.xlsx")

func TestParse(t *testing.T) {
	t.Run("with a valid xlsx file", func(t *testing.T) {
		var handler sync.WaitGroup
		sheet := Sheet{Spreadsheet: testfile, Index: 1}

		workers, err := sheet.Parse(10, &handler)

		expected := Output{Payload{"Foo", "Bar"}, Payload{"Baz", "Quux"}}
		output := receiveOutput(workers[0])

		handler.Wait()

		assertError(t, nil, err)
		assertOutput(t, expected, output)
	})

	t.Run("with empty cells", func(t *testing.T) {
		var handler sync.WaitGroup

		sheet := Sheet{Spreadsheet: testfile, Index: 2}
		workers, err := sheet.Parse(10, &handler)

		handler.Wait()

		assertError(t, nil, err)
		assertZero(t, len(workers))
	})

	t.Run("with chunk defined as zero", func(t *testing.T) {
		var handler sync.WaitGroup

		sheet := Sheet{Spreadsheet: testfile, Index: 1}

		_, err := sheet.Parse(0, &handler)

		handler.Wait()

		assertError(t, ErrUnexpectedChunkSize, err)
	})

	t.Run("when trying to access an out of bound sheet", func(t *testing.T) {
		var handler sync.WaitGroup

		sheet := Sheet{Spreadsheet: testfile, Index: 999}

		_, err := sheet.Parse(10, &handler)

		handler.Wait()

		assertError(t, ErrSheetOutOfBound, err)
	})

	t.Run("with an invalid xlsx file", func(t *testing.T) {
		var handler sync.WaitGroup

		spreadsheet := Spreadsheet("invalidfile.xlsx")
		sheet := Sheet{Spreadsheet: spreadsheet, Index: 1}

		_, err := sheet.Parse(10, &handler)

		assertError(t, ErrFileNotFound, err)
	})
}

func BenchmarkParse(b *testing.B) {
	var handler sync.WaitGroup

	sheet := Sheet{Spreadsheet: testfile, Index: 3}

	workers, _ := sheet.Parse(1000, &handler)

	for _, worker := range workers {
		receiveOutput(worker)
	}

	handler.Wait()
}

func TestConvert(t *testing.T) {
	t.Run("with a sheet containing less than `chunk` lines", func(t *testing.T) {
		sheet := Sheet{Spreadsheet: testfile, Index: 1}
		destination := "/tmp/" + strconv.FormatInt(time.Now().Unix(), 10)
		chunkEvery := uint(100)

		sheet.Convert(destination, chunkEvery)

		assertDirCreated(t, destination)
		assertDirNotEmpty(t, destination)
		assertFileContet(t, destination+"/chunk-0", "Foo;Bar\nBaz;Quux\n")
	})

	t.Run("with an invalid spreadsheet", func(t *testing.T) {
		spreadsheet := Spreadsheet("unknown.xlsx")
		sheet := Sheet{Spreadsheet: spreadsheet, Index: 1}
		destination := "/tmp/" + strconv.FormatInt(time.Now().Unix(), 10)
		chunkEvery := uint(100)

		err := sheet.Convert(destination, chunkEvery)

		assertError(t, ErrFileNotFound, err)
	})

	t.Run("with an invalid destination", func(t *testing.T) {
		sheet := Sheet{Spreadsheet: testfile, Index: 1}
		destination := "/root"
		chunkEvery := uint(100)

		defer func() {
			if err := recover(); err == nil {
				t.Errorf("Expected to panic when unable to write to a file")
			}
		}()

		sheet.Convert(destination, chunkEvery)
	})
}

func BenchmarkConvert(b *testing.B) {
	os.RemoveAll("/tmp/benchmark/")

	b.Run("Executes after cleaning bench dir", func(b *testing.B) {
		sheet := Sheet{Spreadsheet: testfile, Index: 3}
		destination := "/tmp/benchmark/" + strconv.FormatInt(time.Now().Unix(), 10)

		sheet.Convert(destination, 1000)
	})
}

func assertError(t *testing.T, expected, got error) {
	t.Helper()

	if expected != got {
		t.Fatal("Expected to get an error", expected)
	}
}

func assertOutput(t *testing.T, expected, got Output) {
	t.Helper()

	if !reflect.DeepEqual(expected, got) {
		t.Errorf("Expected to output %v, got %v", expected, got)
	}
}

func assertZero(t *testing.T, got int) {
	t.Helper()

	if got != 0 {
		t.Errorf("Expected to get a zero valued integer, got %d", got)
	}
}

func assertDirCreated(t *testing.T, destination string) {
	t.Helper()

	_, err := os.Stat(destination)

	if os.IsNotExist(err) {
		t.Fatal("Expected to create a dir but none was created: ", destination)
	}
}

func assertDirNotEmpty(t *testing.T, destination string) {
	t.Helper()

	f, err := os.Open(destination)
	defer f.Close()

	if err == nil {
		_, err = f.Readdir(1)

		if err == io.EOF {
			t.Fatal("Expected dir not to be empty: ", destination)
		}
	}
}

func assertFileContet(t *testing.T, file string, content string) {
	t.Helper()

	data, err := ioutil.ReadFile(file)

	if err == nil {
		strData := string(data)

		if strData != content {
			t.Errorf("Expected file content to be %q, got %q ", content, strData)
		}
	} else {
		t.Fatal("Expected file to exist: ", file)
	}
}

type Output []Payload

func receiveOutput(worker Worker) Output {
	output := make(Output, worker.to-worker.from)
	times := worker.to - worker.from

	for i := uint(0); i < times; i++ {
		result := <-worker.channel
		output[i] = append(output[i], result...)
	}

	return output
}
