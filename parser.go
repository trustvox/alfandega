package alfandega

import (
	"bufio"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/tealeg/xlsx"
)

// Spreadsheet describes a path to xlsx file
type Spreadsheet string

func (s Spreadsheet) open() (*xlsx.File, error) {
	file, error := xlsx.OpenFile(string(s))

	if error == nil {
		return file, nil
	}

	return nil, ErrFileNotFound
}

// Sheet describes a single sheet with a given index
type Sheet struct {
	Spreadsheet Spreadsheet
	Index       int
}

func (s Sheet) load(file *xlsx.File) (*xlsx.Sheet, error) {
	if s.Index > len(file.Sheets) {
		return nil, ErrSheetOutOfBound
	}

	return file.Sheets[s.Index-1], nil
}

// Convert generates a CSV file from a XLSX one
func (s Sheet) Convert(destination string, chunkEvery uint) error {
	var handler sync.WaitGroup

	workers, err := s.Parse(chunkEvery, &handler)
	errHandler := make(chan error)

	if err != nil {
		return err
	}

	os.MkdirAll(destination, os.ModePerm)

	for _, worker := range workers {
		handler.Add(1)

		// Spawns a new goroutine to separately listen to events
		go func(worker Worker) {
			defer handler.Done()

			filename := destination + "/chunk-" + strconv.Itoa(int(worker.id))
			f, err := os.Create(filename)

			if err != nil {
				errHandler <- err
			}

			defer f.Close()

			writer := bufio.NewWriter(f)

			for i := worker.from; i < worker.to; i++ {
				cells := <-worker.channel
				_, err := writer.WriteString(strings.Join(cells, ";") + "\n")

				if err != nil {
					errHandler <- err
				}
			}

			writer.Flush()

			errHandler <- nil
		}(worker)

		if err := <-errHandler; err != nil {
			panic(err)
		}
	}

	handler.Wait()

	return nil
}

// Parse converts a xlsx sheet into csv
func (s Sheet) Parse(chunkEvery uint, handler *sync.WaitGroup) ([]Worker, error) {
	if chunkEvery == 0 {
		return nil, ErrUnexpectedChunkSize
	}

	file, err := s.Spreadsheet.open()

	if err != nil {
		return nil, err
	}

	sheet, err := s.load(file)

	if err != nil {
		return nil, err
	}

	rowsCount := uint(len(sheet.Rows))
	workers := createWorkers(rowsCount, chunkEvery)

	handler.Add(len(workers))

	for _, worker := range workers {
		go worker.Parse(sheet, handler)
	}

	return workers, nil
}

func createWorkers(rowsCount, chunkEvery uint) []Worker {
	var workers []Worker

	chunkEvery = min(rowsCount, chunkEvery)
	workersCount := rowsCount / max(1, chunkEvery)

	for i := uint(0); i < workersCount; i++ {
		workers = append(workers, CreateWorker(i, chunkEvery))
	}

	return workers
}

// ErrFileNotFound is used when a file couldn't be loaded
var (
	ErrFileNotFound        = ParseErr("File not found")
	ErrSheetOutOfBound     = ParseErr("Attemping to read from out of bound sheet")
	ErrUnexpectedChunkSize = ParseErr("Unable to parse a file with zero chunk")
)

// ParseErr defines the base error of parsing module
type ParseErr string

func (e ParseErr) Error() string {
	return string(e)
}
